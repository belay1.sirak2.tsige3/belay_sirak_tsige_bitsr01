
let profilePic = document.getElementById('profileblue');
let profileBorder = document.getElementById('boxborder');

function hoverOnImage () {
    profilePic.src = "Resources/Images/belay.jpg";
    profileBorder.style.top = "-240px";
    profileBorder.style.left = "10px";
}


function hoverOffImage () {
    profilePic.src = "Resources/Images/belay.jpg";
    profileBorder.style.top = "-220px";
    profileBorder.style.left = "30px";
}


function openExperience(evt, experienceName) {
    var i, tabcontent, tablinks;
  
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
  
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(experienceName).style.display = "block";
    evt.currentTarget.className += " active";
}


